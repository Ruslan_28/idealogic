import React, { Component } from 'react'

import { FormErrors } from './FormErrors'
import AttachFile from './AttachFile'

import './index.scss'

class ContactsForm extends Component {

  state = {
    email: '',
    name: '',
    checkbox: false,
    formErrors: {email: '', name: ''},
    emailValid: false,
    nameValid: false,
    checkboxValid: false,
    formValid: false
  }

  handleUserInput = (e) => {
    const target = e.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name

    this.setState({[name]: value},
      () => { this.validateField(name, value) })
  }

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors
    let emailValid = this.state.emailValid
    let nameValid = this.state.nameValid
    let checkboxValid = this.state.checkbox

    switch(fieldName) {
      case 'email':
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
        fieldValidationErrors.email = emailValid ? '' : ' is invalid'
        break
      case 'name':
        nameValid = value.match(/^[a-zA-Zа-яА-Я'][a-zA-Zа-яА-Я-' ]+[a-zA-Zа-яА-Я']?$/u)
        fieldValidationErrors.name = nameValid ? '' : ' is too short'
        break

      case 'checkbox':
        checkboxValid = value !== false
        fieldValidationErrors.checkbox = checkboxValid ? '' : ' is not checked'
        break

      default:
        break
    }
    this.setState({formErrors: fieldValidationErrors,
      emailValid: emailValid,
      nameValid: nameValid,
      checkboxValid: checkboxValid
    }, this.validateForm)
  }

  validateForm() {
    this.setState({formValid: this.state.emailValid && this.state.nameValid && this.state.checkboxValid})
  }

  errorClass(error) {
    return(error.length === 0 ? '' : 'has-error')
  }

  render() {
    return (
      <form className="form-box">
        <p className="title-form">Send us a message</p>
        <FormErrors formErrors={this.state.formErrors} />

        <input type="email" name="email"
          className={`input ${this.errorClass(this.state.formErrors.email)}`}
          placeholder="Email"
          value={this.state.email}
          onChange={this.handleUserInput}
        />
        <input type="text"  name="name"
          className={`input ${this.errorClass(this.state.formErrors.name)}`}
          placeholder="Name"
          value={this.state.name}
          onChange={this.handleUserInput}
        />

        <input className="input" type="text" placeholder="Your question" />

        <AttachFile />

        <label className="container">I have read and accept
          <input name="checkbox" type="checkbox"
            value={this.state.checkbox}
            onChange={this.handleUserInput}
          />
          <span className="checkmark"></span>
        </label>

        <button className={`submit ${!this.state.formValid ? 'not-allowed' : ''}`} type="submit" disabled={!this.state.formValid}>get in touch</button>
      </form>
    )
  }
}

export default ContactsForm
