import React, { Component } from 'react'

class AttachFile extends Component {

  state = {
    file: 'Attach a file'
  }

  handleChange = (e) => {
    e.preventDefault()
    
    let filesAll = e.target.files[0]

    if(filesAll) {
      let file = filesAll.name
      this.setState({ file })
    } else {
      return
    }
  }

  cutOffString = (string) => {
    if(string.length > 24) {
      return string.substring(0, 24)
    }

    return string
  }

  render() {
    return (
      <div className="upload-btn-wrapper">
        <span className="btn">{ this.cutOffString(this.state.file) }</span>
        <input
          type="file" name="myfile"
          onChange={this.handleChange}
        />
      </div>
    )
  }
}

export default AttachFile
