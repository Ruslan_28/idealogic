const start = require('../../assets/img/start.png')
const head = require('../../assets/img/head.png')
const compass = require('../../assets/img/compass.svg')
const idea = require('../../assets/img/idea.svg')

const vector = require('../../assets/img/vector.png')
const heads = require('../../assets/img/heads.png')
const feature = require('../../assets/img/feature.png')
const ideas = require('../../assets/img/ideas.png')

const initialState = [
  {
    titleBox: 'Who we are?',
    subtitle: 'We are your easiest solution ever.',
    boxContainer: [
      {
        icon: start,
        title: 'Innovation inspired',
        description: 'We hold a hand on a pulse of the newest development tendencies in the digital world. At our disposal, we have various up to date tools to stay on top and provide our customers with the most innovative solutions.'
      },
      {
        icon: head,
        title: 'Open minded',
        description: 'We are the team of out of box thinkers. We are searching for challenging ideas to show how to make the impossible become possible and even real.'
      },
      {
        icon: compass,
        title: 'Client and result oriented',
        description: 'We always value your ideas and your business needs. Explore your ideas in the most innovative way and implement it as your best business practice. Your success is the best reward for us.'
      },
      {
        icon: idea,
        title: 'Technology focused',
        description: 'We know different ways how to make your project perform in today’s competitive marketplace. Our team pursue the highest quality of back-end as well as front-end experience.'
      }
    ]
  },
  {
    titleBox: 'We offer',
    subtitle: null,
    boxContainer: [
      {
        icon: vector,
        title: 'Design',
        description: 'We are the team of out of box thinkers. We are searching for challenging ideas to show how to make the impossible become possible and even real.'
      },
      {
        icon: heads,
        title: 'App development',
        description: 'We are the team of out of box thinkers. We are searching for challenging ideas to show how to make the impossible become possible and even real.'
      },
      {
        icon: feature,
        title: 'Web development',
        description: 'We know different ways how to make your project perform in today’s competitive marketplace. Our team pursue the highest quality of back-end as well as front-end experience.'
      },
      {
        icon: ideas,
        title: 'Blockchain',
        description: 'Our top-notch blockchain development team is ready to build your product. Share your ideas with us and let your business succeed with the most profit-generating technologies.'
      }
    ]
  }
]

export default initialState
