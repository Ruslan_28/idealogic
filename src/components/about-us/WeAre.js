import React, { Component } from 'react';

import './index.scss'

import initialState from './initialState'

class WeAre extends Component {
  state = {
    content: initialState
  }

  render() {
    const { content } = this.state

    return (
      <div>
        {
          content.map((item, index) => {
            return (
                <div key={index}>
                  <p className="title-box">{item.titleBox}</p>
                  <p className="sub-title">{item.subtitle}</p>
                  <div className="content-box">
                    {
                      item.boxContainer.map((current, index) => {
                        return (
                          <div className="box-item" key={index}>
                            <img src={current.icon} alt="decorator"/>
                            <div>
                              <p className="title">{current.title}</p>
                              <p className="description">{current.description}</p>
                            </div>
                          </div>
                        )
                      })
                    }
                  </div>
                </div>
            )
          })
        }
      </div>
    )
  }
}

export default WeAre
