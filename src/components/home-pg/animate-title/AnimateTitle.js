import React, { Component } from 'react';

import './index.scss'

const title = ['young', 'reliable', 'family']

let current = 0

class AnimateTitle extends Component {

  state = {
    slogan: title[0],
    animate: false,
    timer: null
  }

  componentDidMount() {
    const timer = setInterval(() => {
      this.toggleTextSlogan(current)
    }, 5000)

    this.setState({ timer })
  }

  toggleTextSlogan = (index) => {
    this.setState({ animate: true })

    setTimeout(() => {
      this.setState({ slogan: title[current], animate: false })
      current++
      this.setState({ slogan: title[current] })
      if(current>2) {
        current=0
        this.setState({ slogan: title[current] })
      }
    }, 500)
  }

  componentWillUnmount() {
    clearInterval(this.state.timer)
  }

  render() {
    const { animate } = this.state
    const { bgrColors } = this.props

    return (
      <div className="animate-title-box">
        <div className="title-slogan">We are
        {
          bgrColors == null
          ?
          <span className={animate ? 'opacity' : ''}>{this.state.slogan}</span>
          :
          <span className="ideaLog">&lt;idealogic/&gt;</span>
        }
        </div>
        <p className="description-text">One idea can change the world.
        New ideas will discover new words
        </p>
      </div>
    )
  }
}

export default AnimateTitle
