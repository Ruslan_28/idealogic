import React, { Component } from 'react';

import "./index.scss"

import AnimatedText from '../print-text/AnimatedText'

class Preloader extends Component {
  render() {
    const { clas } = this.props

    return (
      <div className={`preloaderBox ${clas}`}>
        <AnimatedText timer={250} text="<idealogic/>" />
      </div>
    )
  }
}
export default Preloader
