import React, { Component } from 'react';

import './index.scss'

const clutch = require('../../../assets/img/clutch.svg')
const linkedin = require('../../../assets/img/linkedin.png')
const facebook = require('../../../assets/img/facebook.svg')
const goodffirms = require('../../../assets/img/goodffirms.svg')

class Social extends Component {
	render() {
		return (
			<div className="social-links">
        <a href="">
          <img src={goodffirms}  alt="decorator"/>
        </a>
				<a href="">
					<img src={clutch}  alt="decorator"/>
				</a>
        <a href="">
					<img src={facebook}  alt="decorator"/>
				</a>
				<a href="">
					<img src={linkedin}  alt="decorator"/>
				</a>
			</div>
		)
	}
}

export default Social
