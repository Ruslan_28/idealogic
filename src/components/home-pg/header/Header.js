import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import './index.scss'

class Header extends Component {
	render() {
    const { colorLogo, toggleClass } = this.props

		return (
			<div className="header-wrapper">
				<div className="title-wrapper">
          <Link to="/">
				      <p className={`title-text ${toggleClass !== undefined ? toggleClass : ''}`}>
                &lt;idea<span className={colorLogo ? colorLogo.clrLogo : ''}>logic</span>/&gt;
              </p>
          </Link>
				</div>

				<div className="links">
					<Link to="/about-us">About as</Link>
					<Link to="">Contact</Link>
					<Link to="">Careers</Link>
				</div>
			</div>
		)
	}
}

export default Header
