import React, { Component } from 'react';

import Links from '../links/Links'

import './index.scss'

class LinksPages extends Component {
	render() {
		const {  title, fields, column, bgrColors, currentIndex } = this.props
    
		return (

			<div className="links-box">
        {
          currentIndex === column &&
          (<div className={`backgroundColors ${bgrColors.bgrClass}`}></div>)
        }
				<p className="title-link-pg">{title}</p>
				{
					fields.map( (link, index) => {
						return (
							<Links
								key={index}
								{...link}
								column={column}
								cell={index}
							/>
						)
					})
				}
			</div>
		)
	}
}

export default LinksPages
