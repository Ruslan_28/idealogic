import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'

import { onToggleHoverLink, offToggleHoverLink } from '../../../redux/actions/linksAction'

import "./index.scss"

class Links extends Component {

	hoverOn = () => {
		const { cell, column } = this.props
		this.props.onToggleHoverLink({ column, cell })
	}

	hoverOff = () => {
		const { cell, column } = this.props
      this.props.offToggleHoverLink({ cell, column })
	}

	render() {
		const { to, text, column, currentIndexObj } = this.props

		return (
			<Link to={to}>
				<div
					className={`link-box ${currentIndexObj === column ? 'hover-link' : ''}`}
					onMouseEnter={this.hoverOn}
					onMouseLeave={this.hoverOff}
				>
					<p>{text}</p>
				</div>
			</Link>
		)
	}
}

export default connect(
	store => ({
    stashDestroyingBanner: store.linksField.stashDestroyingBanner,
    currentIndexObj: store.linksField.currentActiveIndex
  }),
	{ onToggleHoverLink, offToggleHoverLink }
)(Links)
