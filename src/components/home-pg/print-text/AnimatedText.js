import React, { Component } from 'react';

class AnimatedText extends Component {

  state = {
    desiredString: '',
    // intervalBeforeEachChar: 500,
    currentRenderedString: '',
  }

  componentDidMount() {
    this.setDesiredString()
    this.setState({ desiredString: this.props.text})
  }

  componentDidUpdate(oldProps) {
    if(oldProps.text !== this.props.text) {
      this.setState({ desiredString: this.props.text})
    }
  }

  setDesiredString = (string) => {
    const desiredString = string || this.props.text
    const intervalBeforeEachChar = this.props.timer

    this.setState({
      desiredString: desiredString + '',
      // intervalBeforeEachChar,
      currentRenderedString: '',
      // intervalTimer
    })

    const intervalTimer = setInterval(
      () => {
        this.setState( state => {
          const { desiredString, currentRenderedString } = state
          let expectedNextString

          if(state.currentRenderedString.length === desiredString.length) {
            expectedNextString = ''
          } else {
            expectedNextString = desiredString.substring(0, currentRenderedString.length+1)
          }

          return {
            currentRenderedString: expectedNextString,
          }
        })
      },
      intervalBeforeEachChar
    )
  }

  render() {
    const { currentRenderedString } = this.state

    return (
      <span className={`animated-text--typed`}>{ currentRenderedString }</span>
    )
  }
}

export default AnimatedText
