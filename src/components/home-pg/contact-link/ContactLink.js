import React, { Component } from 'react';
import { withRouter } from "react-router";
import PropTypes from "prop-types";
import { Link } from 'react-router-dom'

import "./index.scss"

const contact = require('../../../assets/img/contact us.svg')

class ContactLink extends Component {

  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  render() {
    const { bgrColors, location, } = this.props

    return (
      <div className="slogan-footer">
        <div className="slogan-text">Coming soon
          {
            location.pathname !== "/contacts"
            ?
            <Link to="/contacts" className={`message ${bgrColors ? bgrColors.bgrClass : ''}`}>
              <img src={contact} alt="decorator" />
            </Link>
            :
            null
          }
        </div>
      </div>
    )
  }
}

export default withRouter(ContactLink)
