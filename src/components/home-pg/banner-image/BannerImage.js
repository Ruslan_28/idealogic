import React, { Component } from 'react';
import { connect } from 'react-redux'

import './index.scss'

class BannerImage extends Component {

	state = {
		mounted: false,
		timer: null
	}

	componentDidMount() {
		const timer = setTimeout( () => this.setState({ mounted: true }), 100)
		this.setState({ timer })
	}

	componentWillUnmount() {
		clearTimeout(this.state.timer)
	}

	render() {
		const { activeBanner, stashDestroyingBanner } = this.props.banner

		return (
			<div className={`bannerBox ${this.state.mounted ? 'bgrActive' : ''} ${stashDestroyingBanner ? 'bgrUnactive' : ''}`}>
				<img className={activeBanner.classAdd} src={activeBanner.bannerSrc} alt='decorator'/>
			</div>
		)
	}
}

export default connect(
  state => ({
    banner: state.linksField,
  })
)(BannerImage)
