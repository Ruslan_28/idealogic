import React, { Component } from 'react';

import './index.scss'

import Header from '../../components/home-pg/header/Header'
import Social from '../../components/home-pg/social/Social'
import ContactLink from '../../components/home-pg/contact-link/ContactLink'
import ContactsForm from '../../components/contacts-pg/contacts-form/ContactsForm'

class Contacts extends Component {

	render() {
		return (
      <div className="home-wrapper">
        <div className="home-container reletive">
          <Header />
          <div className="home-center-box">
            <div className="linksContainer">
            <div className="animate-title-box">
              <div className="title-slogan">Contact us</div>
              <p className="description-text">Explore your ideas with us!
              <br/>
              We are open to new challenges!
              </p>
            </div>
            <div className="contacts-description">
              <p className="title-description">Fill im the from or contact us directly:</p>
              <div className="contact-info">
                <div>
                  <p className="title-info">email us:</p>
                  <p className="description-info">ouremail@idealogic.io</p>
                </div>
                <div>
                  <p className="title-info">call us:</p>
                  <p className="description-info">+2004056-77</p>
                </div>
              </div>
            </div>
            </div>
            <Social />
            <ContactLink />
          </div>
          <div className="container-bottom"></div>
          <div className="form-wrapper">
            <ContactsForm />
          </div>
        </div>

      </div>
		)
	}
}

export default Contacts
