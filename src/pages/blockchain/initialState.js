const smartcontract = require('../../assets/img/smartcontract.svg')
const blockchain = require('../../assets/img/blockchain.svg')
const bitcoin = require('../../assets/img/bitcoin.svg')

const initialState = [
  {
    icon: smartcontract,
    title: 'Smart contract development',
    description: 'A basic smart contract principle is created in a way that the execution of contracts is automatic and reliable. It provides decentralization and guarantee transparency of online processes. Trust the smart contract monitor everything for you.'
  },
  {
    icon: blockchain,
    title: 'Blockchain development',
    description: 'We build decentralized applications for Android and iOS platforms suitable for any industry. Our development team helps you to implement the blockchain technology to the application and optimize it for your business needs.'
  },
  {
    icon: bitcoin,
    title: 'Application development',
    description: 'Implementing blockchain technology to our development process help us to offer highly robust and customized solutions to businesses. With a new engineering approach our team of outstanding experts will take your business to the next level.'
  }
]

export default initialState
