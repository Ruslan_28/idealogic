import React, { Component } from 'react';

import './index.scss'

import Header from '../../components/home-pg/header/Header'
import Social from '../../components/home-pg/social/Social'
import initialState from './initialState'

const picture = require('../../assets/img/picture.svg')
const picture_2 = require('../../assets/img/picture_2.svg')

class Blockchain extends Component {
  state = {
    content: initialState
  }

  render() {
    const { content } = this.state

    return (
      <div className="wrapper-container-bg">
        <div className="wrapper-container-box">
          <Header toggleClass={'blockchainHeader'} />

          <div className="blockchain-wrapper">
            <div className="blockchain-top-box">
              <p className="title">Blockchain</p>
              <p className="title-hint">Blockchain development team ready to implement your ideas</p>
              <p className="description">We find solutions for businesses building a secured decentralized system based on private, public or hybrid blockchain network.
              Avoiding centralized data storage, we protect your business from fraud and guarantee safety proved by transparency.</p>
            </div>
            <img src={picture} alt="decorator"/>
            <Social />
            <p className="blockchainTitle">Blockchain</p>
          </div>

          <div className="blockchain-bottom-box">
            <p className="title">Blockchain lab</p>
            <p className="hint-description">Your great ideas are worth sharing, let’s make it real together!</p>

            <div className="blockchain-description-box">
              <img src={picture_2} alt="decorator"/>
              <div className="description-box">
                {
                  content.map((item, index) => {
                    return (
                      <div className="item-description" key={index}>
                        <img src={item.icon} alt="decorator"/>
                        <div>
                          <p className="title">{item.title}</p>
                          <p className="description">{item.description}</p>
                        </div>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Blockchain
