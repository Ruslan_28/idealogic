import React, { Component } from 'react';
import { connect } from "react-redux"

import './index.scss'

import Header from '../../components/home-pg/header/Header'
import Social from '../../components/home-pg/social/Social'
import LinksPages from '../../components/home-pg/link-pages/LinksPages'
import BannerImage from '../../components/home-pg/banner-image/BannerImage'
import Preloader from '../../components/home-pg/preloader/Preloader'
import AnimateTitle from '../../components/home-pg/animate-title/AnimateTitle'
import ContactLink from '../../components/home-pg/contact-link/ContactLink'

const initialState = {
  destroyBg: false,
  activeBannerState: null,
  timer: null,
}

class Home extends Component {

  state = Object.assign({
    toggleRender: false,
    preloader: null,
    destroyPreload: null,
    destroyRender: false
  }, initialState)

  componentDidMount() {
    const preloader = setTimeout( () => this.setState({ toggleRender: true }), 3500)
    const destroyPreload =  setTimeout( () => this.setState({ destroyRender: true }), 3000)
    this.setState({ preloader, destroyPreload })
  }

  componentWillUnmount() {
		clearTimeout(this.state.preloader)
		clearTimeout(this.state.destroyPreload)
	}

	render() {
		const { linksForPages } = this.props.links
		const { activeBanner, stashDestroyingBanner } = this.props.banner
    const { currentIndex, bgrColors, colorLogo } = this.props

    const homePage = <div className="home-wrapper">
        <div className="home-container">
          <Header colorLogo={colorLogo} />
          <div className="home-center-box">
            <div className="linksContainer">
              <AnimateTitle bgrColors={bgrColors}/>
              <div className="linksForPage">
                {
                  linksForPages.map( (linksPages, index) => {
                    return (
                      <LinksPages
                        {...linksPages}
                        key={index}
                        column={index}
                        currentIndex={currentIndex}
                        bgrColors={bgrColors}
                      />
                    )
                  })
                }
              </div>
            </div>
            <Social />
            <ContactLink bgrColors={bgrColors} />
          </div>
          <div className="container-bottom"></div>
        </div>
        {
          (activeBanner || stashDestroyingBanner) &&
            (<BannerImage />)
        }
      </div>

    const loader = <Preloader clas={`${this.state.destroyRender ? 'destroyLoader' : ''}`} />

		return (
			<React.Fragment>
          {
            // this.state.toggleRender ?
              homePage
            // : loader
          }
      </React.Fragment>
		)
	}
}

export default connect(
	state => ({
		links: state.linksField,
		banner: state.linksField,
    currentIndex: state.linksField.currentActiveIndex,
    bgrColors: state.linksField.activeBackground,
    colorLogo: state.linksField.activeClrLogo,
	})
)(Home)
