import React, { Component } from 'react';

import './index.scss'

import Header from '../../components/home-pg/header/Header'
import Social from '../../components/home-pg/social/Social'
import WeAre from '../../components/about-us/WeAre'

class AboutUs extends Component {
  render() {
    return (
      <div className="wrapper-container-bg">
        <div className="wrapper-container-box">
          <Header />
          <div className="wrapper-title-description">
            <div className="about-us-description">
              <p className="title">About us</p>
              <p className="hint-title">Innovative. Challenging. Reliable.</p>
              <p className="description">We are an innovative technology company, specializing in mobile web development, design creation, Blockchain and STO development solutions. We do believe in approaching every project no matter how creative your idea is. Combining our tech expertise with your business needs we ensure that your business will have a profitable competitive advantage.</p>
            </div>
            <Social />
            <p className="logo-title">&lt;idealogic /&gt;</p>
          </div>
          <div className="we-are-wrapper">
            <WeAre />
          </div>
        </div>
      </div>
    )
  }
}

export default AboutUs
