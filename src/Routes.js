import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './pages/home/Home'
import Contacts from './pages/contacts/Contacts'
import AboutUs from './pages/about-us/AboutUs'
import Blockchain from './pages/blockchain/Blockchain'

class Routes extends Component {

  render() {
    return (
      <Router>
        <React.Fragment>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/contacts" component={Contacts} />
            <Route exact path="/about-us" component={AboutUs} />
						<Route exact path="/blockchain" component={Blockchain} />
          </Switch>
        </React.Fragment>
      </Router>
    );
  }

}

export default Routes;
