export function onToggleHoverLink(params) {
	return dispatch => {
		return dispatch({ type: 'TOGGLE_ACTIVE_ON', params })
	}
}

export function offToggleHoverLink(params) {
	return dispatch => {
    dispatch({ type: 'STASH_DESTROYING_BANNER'})
    setTimeout(
      () => dispatch({ type: 'TOGGLE_ACTIVE_OFF', params }),

      500
    )
	}
}
