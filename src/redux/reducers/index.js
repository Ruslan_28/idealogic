import { combineReducers } from 'redux';
import linksField from './linksReducer';

const reducers = {
  linksField,
}

export default combineReducers(
  reducers
)
