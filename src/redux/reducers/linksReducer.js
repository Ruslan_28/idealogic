import _ from 'lodash'
const arrow = require('../../assets/img/arrow.svg')

const mobile = require('../../assets/img/mobile.png')
const ai = require('../../assets/img/ai.png')
const consulting = require('../../assets/img/consulting.png')
const design = require('../../assets/img/design.png')
const iot = require('../../assets/img/iot.png')
const web = require('../../assets/img/web.png')
const zero = require('../../assets/img/zero-stage.png')
const blockchain = require('../../assets/img/blockchain.png')

const initialState = {
	linksForPages: [
		{
			title: 'Services',
			fields: [
				{
					to: 'zero',
					icn: arrow,
					text: 'Zero Stage',
					active: false,
					bannerSrc: zero,
					classAdd: 'zero',
          bgrClass: 'clrGreen',
          clrLogo: 'zeroClr'
				},
				{
					to: 'design',
					icn: arrow,
					text: 'Design',
					active: false,
					bannerSrc: design,
					classAdd: 'design',
          bgrClass: 'clrBlue',
          clrLogo: 'designClr'
				},
				{
					to: 'consulting',
					icn: arrow,
					text: 'Consulting',
					active: false,
					bannerSrc: consulting,
					classAdd: 'consulting',
          bgrClass: 'clrRed',
          clrLogo: 'consultingClr'
				}
			]
		},
		{
			title: 'Development',
			fields: [
				{
					to: 'blockchain',
					icn: arrow,
					text: 'Blockchain',
					active: false,
					bannerSrc: blockchain,
					classAdd: 'blockchain',
          bgrClass: 'clrYellow',
          clrLogo: 'blockchainClr'
				},
				{
					to: 'ai',
					icn: arrow,
					text: 'AI & machine learning',
					active: false,
					bannerSrc: ai,
					classAdd: 'ai',
          bgrClass: 'clrGray',
          clrLogo: 'aiClr'
				},
				{
					to: 'web',
					icn: arrow,
					text: 'WEB',
					active: false,
					bannerSrc: web,
					classAdd: 'web',
          bgrClass: 'clrOrange',
          clrLogo: 'webClr'
				},
				{
					to: 'mobile',
					icn: arrow,
					text: 'Mobile',
					active: false,
					bannerSrc: mobile,
					classAdd: 'mobile',
          bgrClass: 'clrPink',
          clrLogo: 'mobileClr'
				},
        {
					to: 'iot',
					icn: arrow,
					text: 'IoT',
					active: false,
					bannerSrc: iot,
					classAdd: 'iot',
          bgrClass: 'clrGold',
          clrLogo: 'iotClr'
				},
			]
		}
	],
	activeBanner: null,
  stashDestroyingBanner: false,
  currentActiveIndex: null,
  activeBackground: null,
  activeClrLogo: null,
}

export default (state = initialState, action) => {
	const newState = _.cloneDeep(state)

	switch(action.type) {
		case 'TOGGLE_ACTIVE_ON': {
			const { cell, column } = action.params
			const target = newState.linksForPages[column].fields[cell]
			target.active = true
			newState.activeBanner = target
      newState.activeBackground = target
      newState.activeClrLogo = target
      newState.stashDestroyingBanner = false
      newState.currentActiveIndex = column
      return newState

		}

		case 'TOGGLE_ACTIVE_OFF': {
			const { cell, column } = action.params
			const target = newState.linksForPages[column].fields[cell]
			target.active = false
      if(newState.stashDestroyingBanner) {
        newState.stashDestroyingBanner = false
        newState.activeBanner = null
        newState.activeBackground = null
        newState.currentActiveIndex = null
        newState.activeClrLogo = null
      }

      return newState
		}

    case 'STASH_DESTROYING_BANNER': {
      newState.stashDestroyingBanner = true
      return newState
    }

    case 'STOP_STASHING_DESTROYING_BANNER': {
      newState.stashDestroyingBanner = false
      return newState
    }

    default: {
      return newState
    }
  }
}
